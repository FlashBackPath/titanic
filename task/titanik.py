import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_answer_for_group(group_name, group):
    group_count = group['Age'].isnull().sum()
    group_median = group['Age'].dropna().median()
    return (group_name, group_count, round(group_median))

def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''

    mr = df[(df['Sex'] == 'male')]
    res_mr = get_answer_for_group('Mr.', mr)

    miss = df[(df['Sex'] == 'female') & (df['SibSp'] - df['Parch'] > 0)]
    res_miss = get_answer_for_group('Miss.', miss)

    mrs = df[(df['Sex'] == 'female') & ((df['Parch'] > 0) | (df['SibSp'] == 0))]
    res_mrs = get_answer_for_group('Mrs.', mrs)

    return [res_mr, res_mrs, res_miss]

